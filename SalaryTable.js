import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Table,Form,Input,Button} from 'antd';
//import {Empdetails} from './Empdetails';

export class SalaryTable extends Component {
    constructor() {
        super();
        this.state = {
            EmpName:'',
            Designation:'Web developer',
            TotalDays:'15',
            WorkingDays:'',
            LeavesTaken:'',
            CTC:4500,
            TotalSalary:'',
            Savedata:[],
        };
      }
    
      handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
        console.log("name",this.state.EmpName);
      }
    
      handleSubmit = (index) => {
        const   employeedata = [...this.state.Savedata]
        employeedata.push({
              key:index,
              firstName: this.state.EmpName,
              Designation:'Web developer',
              TotalDays:'15',
              WorkingDays: this.state.WorkingDays,
              LeavesTaken: this.state.LeavesTaken,
              CTC:4500,
              TotalSalary:(this.state.WorkingDays-this.state.LeavesTaken)*300
            });
            console.log("copydata",employeedata);
                  this.setState({
                    Savedata: employeedata,
                   
                  });
    }
    render() {
        const columnData = [
            {
              title: 'Name',
              dataIndex: 'firstName',
              key: 'firstName',
            },
            {
              title: 'Designation',
              dataIndex: 'Designation',
              key: 'Designation',
            },
            {
              title: 'TotalDays',
              dataIndex: 'TotalDays',
              key: 'TotalDays',
            },
            {
                title: 'WorkingDays',
                dataIndex: 'WorkingDays',
                key: 'WorkingDays',
              },
              {
                title: 'LeavesTaken',
                dataIndex: 'LeavesTaken',
                key: 'LeavesTaken',
              },
              {
                title: 'CTC',
                dataIndex: 'CTC',
                key: 'CTC',
              },
              {
                title: 'TotalSalary',
                dataIndex: 'TotalSalary',
                key: 'TotalSalary',
              },
          ];
          
        return (
            <div style={{textAlign:'center'}}>
            <Form style={{width:"35%"}}>
            <label>
              Name:
              <Input type="text" name='EmpName'  onChange={this.handleChange} />
              WorkingDays:
              <Input type="text" name='WorkingDays'  onChange={this.handleChange} />
              LeavesTaken:
              <Input type="text" name='LeavesTaken' onChange={this.handleChange} />
            </label>
            <Button type="primary" value="Submit" onClick={this.handleSubmit}>Submit </Button>
          </Form>
          
                <h1>VEDICBAHART TECHNOLOGY SOLUTIONS</h1>
                <h3>Employees Salary Details</h3>
                 <Table style={{border:'2 solid #000'}}
                       bordered
                       columns={columnData}
                       dataSource={this.state.Savedata}
                      />
            </div>
        )
    }
}

export default SalaryTable
